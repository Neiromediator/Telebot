"""Модуль для работы с файлами формата json."""
import json


class Jsoner:
  "Класс для работы с json-объектами."

  def read_json(self, name, encoding='utf-8', *args, **kwargs):
    """Вернёт словарь данных из json-файла.
    Обязательно: имя json-файла.
    По умолчанию: кодировка - encoding='utf-8'
    """
    with open(name, encoding=encoding) as file:
        return json.load(file)
                      
  def write_json(self, params, name, encoding='utf-8', ensure_ascii=False, *args, **kwargs):
    """Запишет словарь данных в json-файл.
    Обязательно: словарь данных для записи, имя json-файла.
    По умолчанию: encoding='utf-8' - кодировка, 
    экранирование не-ASCII символов - ensure_ascii=False.
    """
    with open(name, encoding=encoding, mode='w') as file:
      json.dump(params, file, ensure_ascii=ensure_ascii)
