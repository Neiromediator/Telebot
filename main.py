#import os
#from background import keep_alive # Импорт функции для поддержки работоспособности.
#import pip
#pip.main(['install', 'pytelegrambotapi'])
#from telebot.async_telebot import AsyncTeleBot
import random
import re
import telebot
import time

from cities_class import Cities
from currency_class import Currency
from weather_class import Weather
from values import commands, for_accidental, forex, rand_text

with open('.key') as file:
  key = file.readline()
with open('russian_nouns.txt', encoding='utf-8') as file:
  russian_nouns_lst = file.readlines()
  russian_nouns = ''.join(russian_nouns_lst)
bot = telebot.TeleBot(key)
cities = Cities()
currency = Currency()
wea = Weather()
randomizer = [5]

# Была ли комманда /help?
@bot.message_handler(commands=['help'])
def help_message(message):
  bot.send_message(message.chat.id, commands)
# Был ли запрошен курс валют?
@bot.message_handler(regexp = r'\Aкурс')
def print_cur(message):
  ask_forex = []
  text = message.text.lower().split()
  # При единственном слове или втором слове "валют(..)" - вносим доллар и евро.
  if len(text) == 1 or len(text) == 2 and text[1].lower().startswith("валют"):
    ask_forex = ['USD', 'EUR']
  # При общем запросе - внесём все валюты.
  elif text[1].lower() in ("всего", "общий", "всей", "всех") and len(text) <= 3:
    ask_forex = [forex[i] for i in forex]
  # Иначе определяем запрошенные валюты (но не более 10 слов).
  elif len(text) <= 10:
    for cur in forex:
      # Обрезаем последнюю букву, если имя валюты больше трёх символов.
      cur2 = cur[:-1] if len(cur) > 3 else cur
      if re.search(rf'\b{cur2}', message.text):
        ask_forex.append(forex[cur])
  bot.send_message(message.chat.id, currency.currency_api(ask_forex=ask_forex) if ask_forex else 'Нет данных.')


 # Быдо ли в запросе слово "погод"?   
@bot.message_handler(regexp = 'н?е?погод[аыеу]')
def print_wea(message):
  # Убираем знаки, кроме "-".
  text = ''.join([i if i.isalpha() or i == '-' or i == ' ' else '' for i in message.text])
  text = text.lower().split()
  # Не реагируем на длинный текст.
  if len(text) <= 9:
    city = "Красноярск"
    city_lst = []
    cities_lst = []
    # Был ли запрошен город из двух слов (погода город).
    if len(text) == 2:
      for i in text:
        if not i.startswith("погод") and not i.startswith("непогод"):
          city_lst = cities.define(i)
    # Был ли указан город после "в"?
    for i, word in enumerate(text): 
      if word == "в":
        try:
          city_lst = cities.define(text[i+1])
        except:
          pass
      if city_lst:
        break
    # При 
    if city_lst:
      city, *cities_lst = city_lst
    tomorrow = "завтра" in text or ("послезавтра" in text) * 2
    answ = wea.weather_print(tomorrow=tomorrow, city=city)
    if cities_lst:
      answ = f'{answ}\n\nВозможно вы так же имели ввиду города: {", ".join([i.capitalize() for i in cities_lst])}'
    bot.send_message(message.chat.id, answ)


# Было ли в запосе слово тесть/тестя/тестю?
@bot.message_handler(regexp = 'тест[ьюя]')
def print_answer(message):
  answ = random.choice(rand_text)
  # Задержка ответа на рандомное время до пяти секунд.
  time.sleep(random.random() * 5)
  bot.send_message(message.chat.id, answ) 


# Было ли в запосе слово топ?
@bot.message_handler(regexp = r'\bтопе?(чик)?\b')
def print_top(message):
  text = message.text.lower().split()
  if len(text) <= 7:
    for top in ["топ", "топе", "топчик"]:
      if top in text:
        time.sleep(random.random() * 2)
        bot.reply_to(message, f'ваще {"в " if top=="топе" else ""}{top}')


# Случайный ответ на сообщение с шансом в x%
@bot.message_handler(content_types='text')
def accidental(message):
  randomizer[0] = 100
  if random.random() <= randomizer[0] / 100:
    # Ищем только существительные.
    text = list(filter(lambda x: len(x) >= 4, message.text.lower().split()))
    nouns_lst = []
    for w in text:
      if re.search(rf'\b{w[:-1]}', russian_nouns):
        nouns_lst.append(w)
    # Если есть существительные - изменяем окончание на множественное и сбрасываем шанс ответа.
    if nouns_lst:
      randomizer[0] = 5
      word = random.choice(nouns_lst).lower()
      word = ''.join([i if i.isalpha() else '' for i in word])
      if word[-1] == 'а':
        word = word[:-1]        
      if word[-1] in "бвджзлмнпрстфцш":
        word = word + 'ы'
      elif word.endswith('ок'):
        word = word[:-2] + 'ки'
      elif word.endswith('ец'):
        word = word[:-2] + 'цы'
      elif word[-1] in "гкхчщ":
        word = word + 'и'
      elif word[-1] == "о":
        word = word[:-1] + 'а'
      elif word[-1] in 'йья':
        if word[-1] == "я" and word[-2] in "еилнруь" or word[-1] in "йь":
          word = word[:-1] + 'и'
      # Вместо "*" вставляем наше отобраное слово.
      answer_lst = random.choice(for_accidental).split('*')
      answer = word.join(answer_lst)
      # Делаем заглавную букву, где нужно.
      answer_lst = answer.split('.')
      answer = ''.join([i.capitalize() for i in answer_lst])
      # Выжидаем случайное время и выдаём ответ.
      time.sleep(random.random() * 10)
      bot.reply_to(message, answer)
    # Иначе повышаем шанс на рандомный ответ.
    else:
      randomizer[0] += 3

# Запускаем flask-сервер в отдельном потоке.
#keep_alive()

# Запуск бота.
while True:
  try:
    bot.polling(non_stop=True, interval=0)
  except Exception as e:
    print('!!!!!!error', e)
    time.sleep(30)
