"Класс для получения курса валют."
import requests
import time

from open import Jsoner
from values import forex


class Currency:
  jsoner = Jsoner()
  timer_currency = 0
  rates = {}
  xerof = {v: i for i, v in forex.items()}
  params = jsoner.read_json("params_currency.json")

        
  def currency_api(self, ask_forex=['USD'], *args, **kwargs) -> dict:
    # Обновляем информацию только спустя полчаса.
    if time.time() - self.timer_currency > 1800:
      try:
        URL = 'https://openexchangerates.org/api/latest.json?'
        response = requests.get(URL, params=self.params)
        self.rates = response.json()
        self.timer_currency = time.time()
      except Exception as error:
        return f'Ошибка при запросе валюты: {error}.'
          
    result = ''
    #Пытаемся получить курс для каждой запрошенной валюты и записываем в результат.
    for cur in ask_forex:
      try:
        course = self.rates['rates'].get(cur)
        if course:
          course = round(self.rates['rates']['RUB']/(1 if cur == 'USD' else course), 2)
          result += f'Курс {cur}/RUB: 1 {self.xerof[cur]} = {course}руб.\n'
      except Exception as error:
        result += f'Ошибка {error}, курс {cur} - {self.xerof[cur]} не найден.\n'
    return result
