"""Класс для получения и возврата информации о погоде с сайта openweathermap.org"""
import requests

from open import Jsoner


class Weather:
  """docstring"""
  jsoner = Jsoner()
  params = jsoner.read_json('params_weather.json')
  timer_weather = 0

  def weather_api(self, city):
    """Вернёт словарь данных о погоде (или None и вывод ошибки)."""
    params = self.params
    params['q'] = city
    response = requests.get('https://api.openweathermap.org/data/2.5/forecast?', params=params)
    if response:
      weather_data = response.json()
      return weather_data   
    errors = {401: 'Возможно токен неверный.', 404: 'Возможно город задан неверно.', }
    print(f"Ошибка {response.status_code}: {errors.get(response.status_code, 'Неизвестная ошибка.')}")

  def weather_print(self, tomorrow=0, city="Красноярск"):
    """Вернёт данные о погоде в читаемом виде (или ошибку)."""
    # Проверяем получили ли данные о погоде.
    if weather_all := self.weather_api(city):
      weather_data = weather_all['list'][[0, 8, 16][tomorrow]]
      weather_data_3h = weather_all['list'][1]
      directions = ("северный", "северо-восточный", "восточный", "юго-восточный",
                    "южный", "юго-западный", "западный", "северо-западный")
      day = ["Сейчас", "Завтра", "Послезавтра"][tomorrow]
      try:
        sky = weather_data['weather'][0]['description']
        temp = (weather_data['main']['temp'], weather_data['main']['feels_like'])
        wind = (weather_data['wind']['speed'], weather_data['wind']['gust'])
        direction = directions[int((weather_data['wind']['deg'] + 22.5) // 45 % 8)]
        air = (weather_data['main']['pressure'], weather_data['main']['humidity'])
        sky_3h = weather_data_3h['weather'][0]['description']
        temp_3h = weather_data_3h['main']['temp']
        wea_3h = f"\nЧерез 3 часа ожидается {temp_3h}° и {sky_3h}." if not tomorrow else ''

        return (f"{day} в городе {self.params['q']}: {sky},\n"
                f"Температура {temp[0]}°, ощущается как {temp[1]}°,\n"
                f"Ветер {direction}, {wind[0]}м/с, с порывами до {wind[1]}м/с.,\n"
                f"Давление {int(air[0] / 1.33322)}мм рт/ст, влажность {air[1]}%.{wea_3h}"
                )
      except Exception as error:
        return (f'Ошибка в {error}, {type(error)}.')
