"Класс для нахождения названий городов в строке."
import re

from open import Jsoner


class Cities:
    """docstring"""
    jsoner = Jsoner()
    cities_data = jsoner.read_json('russian_cities.json')
    cities_str = '\n' + '\n'.join(cities_data) + '\n'
    offer = set()
    

    def define(self, word: str) -> list:
        "Вернёт список с макимально подходящими названиями гоородов."
        # Проверка на внегласный Питер.
        if word.startswith('питер'):
            word = 'санкт-петербург'
        word_lst = list(word)
        # Дублируем "е" на "[её]".
        while "е" in word_lst:
            word_lst[word_lst.index('е')] = '[её]'
        # Ищем подходящие названия в списке городов.
        word_ = ''.join(word_lst)
        offer = set(re.findall(fr'\n({word_}.*)\n', self.cities_str, flags=re.IGNORECASE)) \
            | set(re.findall(fr'\n(.*-{word_})\n', self.cities_str, flags=re.IGNORECASE))
        # При единственном варианте - возвращаем результат.
        if len(offer) == 1:
            return list(offer)
        
        # Если нет совпадений, то составляем множество offer с проверкой на возможную опечатку.
        if not offer and len(word) > 2:
            for i, j in enumerate(word_lst):
                word_list_ = word_lst[::]
                word_list_[i] = f'{j}?[а-я]?{j}?'
                city_ = ''.join(word_list_)
                offer = offer | set(re.findall(fr'\n({city_}.*)\n', self.cities_str, flags=re.IGNORECASE))
        # Возвращаем весь список совпадений.
        return list(offer)
